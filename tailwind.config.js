/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
    animation: {
      'opening': 'opening 0.75s linear',
    },
    keyframes: {
      opening: {
        '0%': { opacity: '0' },
        '20%': { opacity: '0' },
        '40%': { opacity: '0.3' },
        '60%': { opacity: '0.5' },
        '80%': { opacity: '0.9' },
        '100%': { opacity: '1' }
      }
    }
  },
  plugins: [],
}
