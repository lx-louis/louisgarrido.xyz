import content from '../../content/Project.json';

export default function Page({ params }) {

  const slug = params.slug; // slug will be "solidnft_plugin" for the first project, for instance

  const project = content.projects.find(proj => proj.slug === slug);
  
  if (!project) return <div>Loading or not found...</div>;

  // Now you have the specific project data
  return (
    <div>
      <h1>{project.name}</h1>
      <p>{project.description}</p>
    </div>
  );
}
