import styles from './Header.module.scss'
import content from '../content/Header.json'

export default function Header() {
    return(
        <div className='w-9/12 text-xl flex flex-col space-y-4'>
            <h1 className={`${styles.header_name} animate-opening`}>{content.name}</h1>
            <h2 className={`${styles.header_position} animate-opening`}>{content.position}</h2>
            <p className={`${styles.header_description} font-light animate-opening`}>
                I build & deploy, accesible and unique projects for <a href='https://paladin.vote/#/' target='_blank'>Paladin</a> with <span className={styles.web3}>web3 interactions</span>.
                I am currently looking for a new opportunity, feel free to contact me !
            </p>
        </div>
    )
}