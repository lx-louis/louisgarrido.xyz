import styles from './About.module.scss'
import content from '../content/About.json'

export default function About() {
    return(
        <div className='mb-16 scroll-mt-16 md:mb-24 lg:mb-36 lg:scroll-mt-24'>
            <div className="sticky top-0 z-20 -mx-6 mb-4 w-screen bg-slate-900/75 
                        px-6 py-5 backdrop-blur md:-mx-12 md:px-12 lg:sr-only 
                        lg:relative lg:top-auto lg:mx-auto lg:w-full lg:px-0 lg:py-0 lg:opacity-0"
            >
                <h2 className="text-sm font-bold uppercase tracking-widest text-slate-200 lg:sr-only">About</h2>
            </div>
            <div className=''>
                <p className='mb-4 leading-8 text-[#94a3b8] text-justify font-light animate-opening'>
                    I am a 23 years old dedicated individual with 5 years of web development
                    experience. I began my journey with the foundational 
                    elements of web development and expanded my skill set 
                    during my 2 year course in Computer Science, where I 
                    was introduced to <span className={styles.react}>the front-end framework, React</span>. I 
                    later ventured into the <span className={styles.blockchain}>blockchain ecosystem</span>, immersing 
                    myself in <span className={styles.eth}>Ethereum development</span>. My drive for knowledge 
                    led me to self-educate, diving deep into various articles, 
                    coding practices, and strengthening my JavaScript skills. 
                    I transitioned to learning React and further delved into 
                    mastering Solidity. Recently, I was recruited by <span className={styles.solid}>Solid NFT 
                    as a Fullstack Developer</span>, where I was developing both internal 
                    and external tools and its customers for a year. Then i was hire by <span className={styles.paladin}>Paladin</span> as a FS Dev also.
                    I am working on multiple project for them, Quest, hPal and more.
                    Besides my professional life, I do (ultra) trail running!
                </p>
            </div>
        </div>
    )
}