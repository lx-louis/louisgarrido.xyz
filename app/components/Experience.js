import Link from 'next/link';
import React from 'react';
import content from '../content/Experience.json';
import styles from './Experience.module.scss'
export default function Experience() {
  return (
    <ul className={styles.experience_list}>
      <div className="sticky top-0 z-20 -mx-6 mb-4 w-screen bg-slate-900/75 px-6 py-5 backdrop-blur md:-mx-12 md:px-12 lg:sr-only lg:relative lg:top-auto lg:mx-auto lg:w-full lg:px-0 lg:py-0 lg:opacity-0">
        <h2 className="text-sm font-bold uppercase tracking-widest text-slate-200 lg:sr-only">Experience</h2>
      </div>
      {content.experience.map((exp, index) => (
        <li key={index} className="mb-16 scroll-mt-16 md:mb-22 lg:mb-28 lg:scroll-mt-20 w-[100%] animate-opening">
          <div className={styles.experience_item_date}>
            <h4 className={styles.experience_date_data}>{exp.duration}</h4>
          </div>
          <div className={styles.experience_item_content}>
            <div className={styles.experience_content_up}>
              <h3>{exp.position}</h3>
              {exp.company_link ? 
                (  
                  <Link 
                    href={exp.company_link} 
                    target="_blank"
                  >
                    <h4>{exp.company}</h4>
                  </Link>
                ) : (
                  <h4>{exp.company}</h4>
                )
              }
            </div>
            <p className={styles.experience_content_middle}>
              {exp.description}
            </p>
            <div className={styles.experience_content_bottom}>
                <ul className={styles.experience_skills_list}>
                  {exp.skills.map((technology, techIndex) => (
                    <li key={techIndex} className={styles.experience_technology}>
                      {technology}
                    </li>
                  ))}
                </ul>
              </div>
          </div>
        </li>
      ))}
    </ul>
  );
}
