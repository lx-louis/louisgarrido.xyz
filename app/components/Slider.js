import About from "./About"
import Experience from "./Experience"
import Project from "./Project"

export default function Slider() {
    return(
        <div className='pt-24 lg:w-full lg:py-24'>
            <About/>
            <Experience/>
            <Project/>
        </div>
    )
}