import styles from './Footer.module.scss'
import content from '../content/Footer.json'

export default function Footer() {
    return(
        <div className={styles.footer}>
            <p className={styles.content}>
                {content.footer}
            </p>
        </div>
    )
}