import React from 'react';
import styles from './Project.module.scss';
import content from '../content/Project.json';
import Image from 'next/image';
import Link from 'next/link';
//<Link key={index} href={`/projects/${project.slug}`} passHref>

export default function Project() {
  return (
    <div className={styles.project}>
      <ul className={styles.projects_list}>
        <div className="sticky top-0 z-20 -mx-6 mb-4 w-screen bg-slate-900/75 px-6 py-5 backdrop-blur md:-mx-12 md:px-12 lg:sr-only lg:relative lg:top-auto lg:mx-auto lg:w-full lg:px-0 lg:py-0 lg:opacity-0">
          <h2 className="text-sm font-bold uppercase tracking-widest text-slate-200 lg:sr-only">Projects</h2>
        </div>
        
        {content.projects.map((project, index) => (
          <>
            {project.link && project.link !== "0" ? (
              <Link key={index} href={project.link} target="_blank">
                <li className="mb-14 scroll-mt-16 md:mb-20 lg:mb-30 lg:scroll-mt-20 flex w-[100%] animate-opening">
                  <div className={styles.projects_item_image}>
                    <Image className={styles.projects_img} src={`/${project.thumbail}.png`} alt={project.name} width={300} height={300} />
                  </div>
                  
                  <div className={styles.projects_item_content}>
                    <div className={styles.projects_content_up}>
                      <h2>{project.name}</h2>
                    </div>
                    
                    <p className={styles.projects_content_middle}>
                      {project.description}
                    </p>
                    
                    <div className={styles.projects_content_bottom}>
                      <ul className={styles.projects_technologies_list}>
                        {project.technologies.map((technology, techIndex) => (
                          <li key={techIndex} className={styles.projects_technology}>
                            {technology}
                          </li>
                        ))}
                      </ul>
                    </div>
                    
                  </div>
                </li>
              </Link>
              ) : (
                <li key={index} className="mb-14 scroll-mt-16 md:mb-20 lg:mb-30 lg:scroll-mt-20 flex w-[100%] animate-opening">
                
                <div className={styles.projects_item_image}>
                  <Image className={styles.projects_img} src={`/${project.thumbail}.png`} alt={project.name} width={300} height={300} />
                </div>
                
                <div className={styles.projects_item_content}>
                  <div className={styles.projects_content_up}>
                    <h2>{project.name}</h2>
                  </div>
                  
                  <p className={styles.projects_content_middle}>
                    {project.description}
                  </p>
                  
                  <div className={styles.projects_content_bottom}>
                    <ul className={styles.projects_technologies_list}>
                      {project.technologies.map((technology, techIndex) => (
                        <li key={techIndex} className={styles.projects_technology}>
                          {technology}
                        </li>
                      ))}
                    </ul>
                  </div>
                  
                </div>
              </li>
              )}
            </>
          //</Link>
        ))}
      </ul>
    </div>
  );
}
